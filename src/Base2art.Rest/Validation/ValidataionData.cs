﻿namespace Base2art.Rest.Validation
{
    using System;

    [Serializable]
    public class ValidataionData : IFieldValidationData
    {
        private readonly string field;
        private readonly string validationType;
        private readonly string defaultMessageText;

        public ValidataionData(string field, string validationType, string defaultMessageText)
        {
            this.field = field;
            this.validationType = validationType;
            this.defaultMessageText = defaultMessageText;
        }

        public string Field
        {
            get
            {
                return this.field;
            }
        }

        public string ValidationType
        {
            get
            {
                return this.validationType;
            }
        }

        public string DefaultMessageText
        {
            get
            {
                return this.defaultMessageText;
            }
        }
    }
}