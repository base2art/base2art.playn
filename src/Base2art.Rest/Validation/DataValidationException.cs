﻿namespace Base2art.Rest.Validation
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using System.Security.Permissions;
    using System.Text;

    [Serializable]
    public class DataValidationException : ApplicationException
    {
        private readonly IValidationDataCollection messages;

        public DataValidationException(ValidationDataCollection messages)
            : base(Convert(messages))
        {
            this.messages = messages;
        }

        public DataValidationException(ValidationDataCollection messages, Exception innerException)
            : base(Convert(messages), innerException)
        {
            this.messages = messages;
        }

        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        protected DataValidationException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            this.messages = (IValidationDataCollection)info.GetValue("DataValidationException.Messages", typeof(ValidationDataCollection));
        }

        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValue("DataValidationException.Messages", this.Messages, typeof(ValidationDataCollection));
        }

        public IValidationDataCollection Messages
        {
            get
            {
                return this.messages;
            }
        }

        protected static string Convert(IValidationDataCollection messages)
        {
            if (messages == null || !messages.HasErrors)
            {
                return "Generic Error";
            }

            StringBuilder sb = new StringBuilder();
            Append(sb, messages.GlobalErrors, "GLOBAL");
            Append(sb, messages.SystemErrors, "SYSTEM");
            Append(sb, messages.ObjectErrors, "OBJECT");
            Append(sb, messages.FieldErrors);


            return sb.ToString();
        }

        private static void Append(StringBuilder sb, IEnumerable<IFieldValidationData> fieldErrors)
        {
            foreach (IFieldValidationData vd in fieldErrors)
            {
                sb.Append("Type: '");
                sb.Append(vd.ValidationType);
                sb.Append(" (");
                sb.Append(vd.Field);
                sb.Append(")");
                sb.Append("' ");

                sb.Append("Message: '");
                sb.Append(vd.DefaultMessageText);
                sb.Append("' ");
                sb.Append("\n");
            }
        }

        private static void Append(StringBuilder sb, IEnumerable<IValidationData> errorMessages, string text)
        {
            foreach (IValidationData vd in errorMessages)
            {
                sb.Append("Type: '");
                sb.Append(text);
                sb.Append("' ");

                sb.Append("Message: '");
                sb.Append(vd.DefaultMessageText);
                sb.Append("' ");
                sb.Append("\n");
            }
        }
    }
}


/*

        public DataValidationException(
          IValidationDataCollection messages,
          Throwable cause,
          boolean enableSuppression,
          boolean writableStackTrace)
        {
            super(
              Convert(messages),
              cause,
              enableSuppression,
              writableStackTrace);
            this.messages = messages;
        }

        public DataValidationException(DataValidationException cause)
        {
            super(cause);
            this.messages = new ValidationDataCollection();
        }

        public DataValidationException(IValidationDataCollection messages, DataValidationException cause)
        {
            super(Convert(messages), cause);
            this.messages = messages;
        }

        public DataValidationException(IValidationDataCollection messages)
        {
            super(Convert(messages));
            this.messages = messages;
        }

        public DataValidationException()
        {
            this.messages = new ValidationDataCollection();
        }
*/