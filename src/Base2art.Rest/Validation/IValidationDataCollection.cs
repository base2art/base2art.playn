﻿namespace Base2art.Rest.Validation
{
    using System.Collections.Generic;

    public interface IValidationDataCollection
    {
        IEnumerable<IValidationData> GlobalErrors { get; }

        IEnumerable<IValidationData> SystemErrors { get; }

        IEnumerable<IValidationData> ObjectErrors { get; }

        IEnumerable<IFieldValidationData> FieldErrors { get; }

        IEnumerable<IValidationData> AllErrors { get; }

        bool HasErrors { get; }

        int Count { get; }
    }
}