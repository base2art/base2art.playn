﻿namespace Base2art.Rest.Validation
{
    public interface IUserAccessValidator<in TKey, in TModel, in TUserData>
    {
        bool Can(TUserData userData, string verb);

        // throws DataAccessException;
        void InsistThat(TUserData userData, string verb);
        
        bool CanForKey(TUserData userData, string verb, TKey key);

        // throws DataAccessException;
        void InsistForKeyThat(TUserData userData, string verb, TKey key);

        bool CanForModel(TUserData userData, string verb, TModel model);

        // throws DataAccessException;
        void InsistForModelThat(TUserData userData, string verb, TModel model);
    }
}
