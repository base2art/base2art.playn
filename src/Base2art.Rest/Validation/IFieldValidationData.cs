﻿namespace Base2art.Rest.Validation
{
    public interface IFieldValidationData : IValidationData
    {
        string Field { get; }
    }
}