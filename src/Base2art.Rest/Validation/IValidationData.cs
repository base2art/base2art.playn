﻿namespace Base2art.Rest.Validation
{
    public interface IValidationData
    {
        string ValidationType { get; }

        string DefaultMessageText { get; }
    }
}