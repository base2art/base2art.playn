﻿namespace Base2art.Rest.Validation
{
    public enum ValidationDataStandarErrorTypes
    {
        Required,
        OutOfRange,
        PermissionError,
        IntegrityError,
        FormatError,
        DuplicateKeys,
        DuplicateItems,

        // TODO ADD MORE...
        InvalidUsername,
        InvalidUserPassword
    }
}