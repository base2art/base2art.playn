﻿namespace Base2art.Rest.Validation
{
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class ValidationDataCollection : IValidationDataCollection
    {
        private readonly List<IFieldValidationData> fieldErrors = new List<IFieldValidationData>();

        private readonly List<IValidationData> objectErrors = new List<IValidationData>();

        private readonly List<IValidationData> systemErrors = new List<IValidationData>();

        private readonly List<IValidationData> globalErrors = new List<IValidationData>();

        public bool HasErrors
        {
            get
            {
                return this.Count > 0;
            }
        }

        public int Count
        {
            get
            {
                return this.fieldErrors.Count + this.objectErrors.Count + this.systemErrors.Count
                       + this.globalErrors.Count;
            }
        }

        public IEnumerable<IValidationData> GlobalErrors
        {
            get
            {
                return this.globalErrors;
            }
        }

        public IEnumerable<IValidationData> SystemErrors
        {
            get
            {
                return this.systemErrors;
            }
        }

        public IEnumerable<IValidationData> ObjectErrors
        {
            get
            {
                return this.objectErrors;
            }
        }

        public IEnumerable<IFieldValidationData> FieldErrors
        {
            get
            {
                return this.fieldErrors;
            }
        }

        public IList<IFieldValidationData> FieldErrorData
        {
            get
            {
                return this.fieldErrors;
            }
        }

        public IList<IValidationData> GlobalErrorData
        {
            get
            {
                return this.globalErrors;
            }
        }

        public IList<IValidationData> ObjectErrorData
        {
            get
            {
                return this.objectErrors;
            }
        }

        public IList<IValidationData> SystemErrorData
        {
            get
            {
                return this.systemErrors;
            }
        }

        public IEnumerable<IValidationData> AllErrors
        {
            get
            {
                foreach (var validationData in this.FieldErrors)
                {
                    yield return validationData;
                }

                foreach (var validationData in this.ObjectErrors)
                {
                    yield return validationData;
                }

                foreach (var validationData in this.SystemErrors)
                {
                    yield return validationData;
                }

                foreach (var validationData in this.GlobalErrors)
                {
                    yield return validationData;
                }
            }
        }

        public IFieldValidationData AddFieldError(string fieldName, string errorType, string defaultMessage)
        {
            ValidataionData validataionData = new ValidataionData(fieldName, errorType, defaultMessage);
            this.fieldErrors.Add(validataionData);
            return validataionData;
        }

        public IFieldValidationData AddFieldError(string fieldName, ValidationDataStandarErrorTypes errorType, string defaultMessage)
        {
            return this.AddFieldError(fieldName, errorType.ToString("G"), defaultMessage);
        }

        public IValidationData AddGlobalError(string errorType, string defaultMessage)
        {
            ValidataionData validataionData = new ValidataionData(null, errorType, defaultMessage);
            this.globalErrors.Add(validataionData);
            return validataionData;
        }

        public IValidationData AddGlobalError(ValidationDataStandarErrorTypes errorType, string defaultMessage)
        {
            return this.AddGlobalError(errorType.ToString("G"), defaultMessage);
        }

        public IValidationData AddObjectError(string errorType, string defaultMessage)
        {
            ValidataionData validataionData = new ValidataionData(null, errorType, defaultMessage);
            this.objectErrors.Add(validataionData);
            return validataionData;
        }

        public IValidationData AddObjectError(ValidationDataStandarErrorTypes errorType, string defaultMessage)
        {
            return this.AddObjectError(errorType.ToString("G"), defaultMessage);
        }

        public IValidationData AddSystemError(string errorType, string defaultMessage)
        {
            ValidataionData validataionData = new ValidataionData(null, errorType, defaultMessage);
            this.systemErrors.Add(validataionData);
            return validataionData;
        }

        public IValidationData AddSystemError(ValidationDataStandarErrorTypes errorType, string defaultMessage)
        {
            return this.AddSystemError(errorType.ToString("G"), defaultMessage);
        }
    }
}
