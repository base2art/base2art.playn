﻿namespace Base2art.Rest
{
    using Base2art.Rest.Models;

    public interface IReadByCriteriaService<TReturn, in TFindOptions, in TUserData>
    {
        TReturn[] AllByCriteria(TFindOptions options, TUserData userData);

        IPagedList<TReturn> FindByCriteria(TFindOptions options, PaginationData paginationData, TUserData userData);
    }
}