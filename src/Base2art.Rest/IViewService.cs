﻿namespace Base2art.Rest
{
    public interface IViewService<TKey, TReturn, in TUserData>
        : IPrimaryKeyService<TKey, TReturn>
    {
        TReturn GetById(TKey id, TUserData userData);
    }
}