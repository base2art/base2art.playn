﻿namespace Base2art.Rest
{
    public interface IReadViewCreateEditService<TKey, TReturn, in TFindOptions, in TData, in TUserData>
        : 
            IEditService<TKey, TReturn, TData, TUserData>,
            IReadViewCreateService<TKey, TReturn, TFindOptions, TData, TUserData>
    {
    }
}