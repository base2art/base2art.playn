﻿namespace Base2art.Rest
{
    public interface IReadViewCreateDeleteService<TKey, TReturn, in TFindOptions, in TData, in TUserData>
      :
        IReadViewCreateService<TKey, TReturn, TFindOptions, TData, TUserData>,
        IViewCreateDeleteService<TKey, TReturn, TData, TUserData>
    {
    }
}
