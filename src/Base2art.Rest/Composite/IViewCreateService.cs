﻿namespace Base2art.Rest
{
    public interface IViewCreateService<TKey, TReturn, in TData, in TUserData>
        :
            IViewService<TKey, TReturn, TUserData>,
            ICreateService<TKey, TReturn, TData, TUserData>
    {
    }
}