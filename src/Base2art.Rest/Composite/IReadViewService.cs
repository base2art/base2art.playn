﻿namespace Base2art.Rest
{
    public interface IReadViewService<TKey, TReturn, in TFindOptions, in TUserData>
        :
            IReadService<TReturn, TUserData>,
            IReadByCriteriaService<TReturn, TFindOptions, TUserData>,
            IViewService<TKey, TReturn, TUserData>
    {
    }
}