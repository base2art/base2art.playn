﻿namespace Base2art.Rest
{
    public interface IReadViewCreateEditDeleteService<TKey, TReturn, in TFindOptions, in TData, in TUserData>
        :
           IReadViewCreateDeleteService<TKey, TReturn, TFindOptions, TData, TUserData>,
            IReadViewCreateEditService<TKey, TReturn, TFindOptions, TData, TUserData>
    {
    }
}