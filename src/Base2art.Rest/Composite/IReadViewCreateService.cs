﻿namespace Base2art.Rest
{
    public interface IReadViewCreateService<TKey, TReturn, in TFindOptions, in TData, in TUserData>
        :
            IReadViewService<TKey, TReturn, TFindOptions, TUserData>,
            IViewCreateService<TKey, TReturn, TData, TUserData>
    {
    }
}