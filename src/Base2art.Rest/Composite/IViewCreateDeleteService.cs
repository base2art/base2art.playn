﻿namespace Base2art.Rest
{
    public interface IViewCreateDeleteService<TKey, TReturn, in TData, in TUserData>
        :
            IDeleteService<TKey, TUserData>,
            IViewCreateService<TKey, TReturn, TData, TUserData>
    {
    }
}