﻿namespace Base2art.Rest
{
    public interface ICreateService<out TKey, TReturn, in TData, in TUserData>
        : IPrimaryKeyService<TKey, TReturn>
    {
        TReturn Save(TData content, TUserData userData);
    }
}
