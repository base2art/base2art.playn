﻿namespace Base2art.Rest.Models
{
    public interface IModel<out TKey>
    {
        TKey Id { get; }
    }
}
