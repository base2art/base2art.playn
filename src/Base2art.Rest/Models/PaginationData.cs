namespace Base2art.Rest.Models
{
    public class PaginationData
    {
        public PaginationData()
        {
        }

        public PaginationData(int pageIndex, int pageSize)
        {
            this.PageIndex = pageIndex;
            this.PageSize = pageSize;
        }

        public int PageIndex { get; set; }

        public int PageSize { get; set; }

        public int Start
        {
            get
            {
                return this.PageIndex * this.PageSize;
            }
        }

        public int End
        {
            get
            {
                return ((this.PageIndex + 1) * this.PageSize) - 1;
            }
        }
    }
}