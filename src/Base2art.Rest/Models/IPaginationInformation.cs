﻿namespace Base2art.Rest.Models
{
    public interface IPaginationInformation
    {
        int Start { get; }

        int End { get; }

        int PageIndex { get; }

        int PageSize { get; }

        int TotalCount { get; }

        bool HasPreviousPage { get; }

        bool HasNextPage { get; }

        IPaginationInformation NextPage { get; }

        IPaginationInformation PreviousPage { get; }

        PaginationData PageData { get; }
    }
}