﻿namespace Base2art.Rest.Models
{
    using System.Collections;
    using System.Collections.Generic;

    public class PagedList<T> : IPagedList<T>
    {
        private readonly T[] items;

        private readonly PaginationData paginationData;

        private readonly int totalCount;

        public PagedList(T[] items, PaginationData paginationData, int totalCount)
        {
            items.Validate().IsNotNull();
            paginationData.Validate().IsNotNull();
            totalCount.Validate().GreaterThanOrEqualTo(0);

            items.Length.Validate().LessThanOrEqualTo(paginationData.PageSize);

            this.items = items;
            this.paginationData = paginationData;
            this.totalCount = totalCount;
        }

        public IPaginationInformation PaginationInformation
        {
            get
            {
                return new PaginationInformation(this.paginationData, this.totalCount);
            }
        }

        public T[] Data
        {
            get
            {
                return this.items;
            }
        }

        public int Count
        {
            get
            {
                return this.items.Length;
            }
        }

        public int TotalCount
        {
            get
            {
                return this.totalCount;
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            foreach (var item in this.items)
            {
                yield return item;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}