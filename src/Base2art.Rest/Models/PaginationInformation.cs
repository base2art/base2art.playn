﻿namespace Base2art.Rest.Models
{
    using System;

    public class PaginationInformation : IPaginationInformation
    {
        private readonly PaginationData paginationData;

        private readonly int totalCount;

        public PaginationInformation(PaginationData paginationData, int totalCount)
        {
            this.paginationData = paginationData;
            this.totalCount = totalCount;
        }

        public int Start
        {
            get
            {
                var start = this.paginationData.Start;
                return start < 0 ? 0 : start;
            }
        }

        public int End
        {
            get
            {
                int totalCountEnd = this.totalCount - 1;
                if (totalCountEnd <= this.paginationData.End)
                {
                    return totalCountEnd;
                }

                return this.paginationData.End;
            }
        }

        public int PageIndex
        {
            get
            {
                return this.paginationData.PageIndex;
            }
        }

        public int PageSize
        {
            get
            {
                return this.paginationData.PageSize;
            }
        }

        public int TotalCount
        {
            get
            {
                return this.totalCount;
            }
        }

        public bool HasPreviousPage
        {
            get
            {
                return this.PageIndex > 0;
            }
        }

        public bool HasNextPage
        {
            get
            {
                int totalCountEnd = this.totalCount - 1;
                return totalCountEnd > this.End;
            }
        }

        public IPaginationInformation NextPage
        {
            get
            {
                if (!this.HasNextPage)
                {
                    throw new InvalidOperationException("Bad Page");
                }

                var pdata = new PaginationData { PageSize = this.PageSize, PageIndex = this.PageIndex + 1 };
                return new PaginationInformation(pdata, this.totalCount);
            }
        }

        public IPaginationInformation PreviousPage
        {
            get
            {
                if (!this.HasPreviousPage)
                {
                    throw new InvalidOperationException("Bad Page");
                }

                var pdata = new PaginationData { PageSize = this.PageSize, PageIndex = this.PageIndex - 1 };
                return new PaginationInformation(pdata, this.totalCount);
            }
        }

        public PaginationData PageData
        {
            get
            {
                return this.paginationData;
            }
        }
    }
}