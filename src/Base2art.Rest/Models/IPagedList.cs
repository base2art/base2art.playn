﻿namespace Base2art.Rest.Models
{
    using System.Collections.Generic;

    public interface IPagedList<out T> : IEnumerable<T>
    {
        IPaginationInformation PaginationInformation { get; }

        T[] Data { get; }

        int Count { get; }

        int TotalCount { get; }
    }
}