﻿namespace Base2art.Rest
{
    public interface IDeleteService<in TKey, in TUserData>
    {
        bool Delete(TKey id, TUserData userData);
    }
}