﻿namespace Base2art.Rest
{
    public interface IEditService<TKey, TReturn, in TData, in TUserData>
        : IViewService<TKey, TReturn, TUserData>
    {
        TReturn Update(TKey id, TData content, TUserData userData);
    }
}