﻿namespace Base2art.Rest
{
    using Base2art.Rest.Models;

    public interface IReadService<out TReturn, in TUserData>
    {
        TReturn[] All(TUserData userData);

        IPagedList<TReturn> Find(PaginationData paginationData, TUserData userData);
    }
}