﻿namespace Base2art.Rest
{
    public interface IPrimaryKeyService<out TKey, in TServiceReturn>
    {
        TKey GetId(TServiceReturn serviceReturn);
    }
}