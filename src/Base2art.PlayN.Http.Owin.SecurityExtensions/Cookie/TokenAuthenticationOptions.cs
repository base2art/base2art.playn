﻿namespace Base2art.PlayN.Http.Owin.Security
{
	public class TokenAuthenticationOptions
	{
	    private string tokenName;
	    
		public string TokenName
		{
		    get
		    {
		        if (string.IsNullOrWhiteSpace(tokenName))
		        {
		            return "user";
		        }
		        
		        return this.tokenName;
		    }
			set
			{
			    this.tokenName = value;
			}
		}
	}
}


/*
 
//
//		public ISecureDataFormat<Microsoft.Owin.Security.AuthenticationProperties> StateDataFormat
//		{
//			get;
//			set;
//		}
 
		public TokenAuthenticationOptions()
		{
//			Description.Caption = Constants.DefaultAuthenticationType;
//			CallbackPath = new PathString("/signin-dummy");
//			AuthenticationMode = AuthenticationMode.Active;
		}
//
//		public PathString CallbackPath
//		{
//			get;
//			set;
//		}
//
//		public string UserName
//		{
//			get;
//			set;
//		}
//
//		public string UserId
//		{
//			get;
//			set;
//		}
 
 */