﻿namespace Base2art.PlayN.Http.Owin.Security
{
	using global::Owin;
	
	public static class TokenAuthenticationHandlerExtender
	{
		public static global::Owin.IAppBuilder UseTokenAuthentication(this global::Owin.IAppBuilder app, TokenAuthenticationOptions options)
		{
		    return app.Use<TokenAuthenticationMiddleware>(options);
		}
	}
}



