﻿namespace Base2art.PlayN.Http.Owin.Security
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;
    using System.Security.Principal;
    using Base2art.Collections.Generic;
    using Base2art.Net;
    using Microsoft.Owin;
//	using Base2art.Net;

    public class TokenAuthenticationMiddleware : OwinMiddleware
    {
        public const string NameKey = "n";
        
        public const string GroupKey = "g";
        
        public const string CookieAuthenticationType = "PlayN:Cookie";
        
        private readonly TokenAuthenticationOptions options;

        public TokenAuthenticationMiddleware(OwinMiddleware next)
            : base(next)
        {
        }
        
        public TokenAuthenticationMiddleware(OwinMiddleware next, TokenAuthenticationOptions options)
            : base(next)
        {
            this.options = options;
        }
        
        public async override System.Threading.Tasks.Task Invoke(IOwinContext context)
        {
            var settings = new SecureCookieJarSettings{
                ApplicationSaltSettings = "23fcbedb-5b64-490d-977e-5731a0111ed2",
                SecureCookiePrefix = "scs"
            };
            
            var cookieJar = new OwinCookieJar(context, settings);
            
            var mapTempUserColl = cookieJar.GetSecureCookieValues(this.options.TokenName);
            
            var name = GetUserName(mapTempUserColl);
            var groups = GetGroupNames(mapTempUserColl);
            
//            var identityTypesToPreserve = new HashSet<string>{CookieAuthenticationType};
            
            //            if (inboundClaimsPrincipal != null)
            //            {
            //                foreach (var identity in inboundClaimsPrincipal.Identities)
            //                {
//
            //                }
            //            }
            
            if (!string.IsNullOrWhiteSpace(name))
            {
                var inboundClaimsPrincipal = this.AuthenticatedUser(context.Request.User);
                
                if (inboundClaimsPrincipal != null)
                {
                    if (string.Equals(inboundClaimsPrincipal.Identity.Name, name))
                    {
                        ClaimsIdentity identity = CreateFormsIdentity(name, groups);
                        
                        var items = new List<ClaimsIdentity>();
                        items.AddRange(inboundClaimsPrincipal.Identities);
                        items.Add(identity);
                        context.Request.User = new ClaimsPrincipal(items);
                    }
                    else
                    {
                        // Do not update the User as the user from the identity framework
                        // takes precedence over forms.
                    }
                }
                else
                {
                    context.Request.User = new ClaimsPrincipal(CreateFormsIdentity(name, groups));
                }
            }
            
            await Next.Invoke(context);
            
            var claimsPrincipal = this.AuthenticatedUser(context.Request.User);
            if (claimsPrincipal != null)
            {
                var mm = new MultiMap<string, string>();
                mm.Add(NameKey, claimsPrincipal.Identity.Name);
                
                foreach (var identity in claimsPrincipal.Identities)
                {
                    foreach (var claim in identity.Claims.Where(x=>x.Type == identity.RoleClaimType))
                    {
                        mm.Add(GroupKey, claim.Value);
                    }
                }
                
                cookieJar.SetSecureCookieValues(this.options.TokenName, mm);
            }
        }

        public static string GetUserName(IReadOnlyMultiMap<string, string> mapTempUserColl)
        {
            string name = string.Empty;
            if (mapTempUserColl.Contains(NameKey))
            {
                var names = (mapTempUserColl[NameKey] ?? new string[0]).ToArray();
                if (names.Length == 1)
                {
                    name = names[0];
                }
            }
            
            return name;
        }

        public static string[] GetGroupNames(IReadOnlyMultiMap<string, string> mapTempUserColl)
        {
            string[] groups = new string[0];
            if (mapTempUserColl.Contains(GroupKey))
            {
                groups = (mapTempUserColl[GroupKey] ?? new string[0]).Where(x => !string.IsNullOrWhiteSpace(x)).ToArray();
            }
            return groups;
        }
        
        private ClaimsPrincipal AuthenticatedUser(IPrincipal principal)
        {
            if (principal != null &&
                principal.Identity != null &&
                principal.Identity.IsAuthenticated &&
                !string.IsNullOrWhiteSpace(principal.Identity.Name))
            {
                return principal as ClaimsPrincipal;
            }
            
            return null;
        }

        private static ClaimsIdentity CreateFormsIdentity(string name, string[] groups)
        {
            var identity = new ClaimsIdentity(CookieAuthenticationType, ClaimTypes.NameIdentifier, ClaimTypes.Role);
            identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, name));
            foreach (var group in groups)
            {
                identity.AddClaim(new Claim(ClaimTypes.Role, group));
            }
            
            return identity;
        }
    }
}