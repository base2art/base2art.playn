﻿namespace Base2art.PlayN.Http.Owin.Security.Windows
{
	using System.Net;
	using global::Owin;

	public static class WindowsAuthenticationMiddlewareHandlerExtender
	{
		public static global::Owin.IAppBuilder UseWindowsAuthentication(this global::Owin.IAppBuilder app)
		{
            var listener = (HttpListener) app.Properties["System.Net.HttpListener"];
            listener.AuthenticationSchemes = AuthenticationSchemes.IntegratedWindowsAuthentication;
			return app.Use<WindowsAuthenticationMiddleware>();
		}
	}
}


