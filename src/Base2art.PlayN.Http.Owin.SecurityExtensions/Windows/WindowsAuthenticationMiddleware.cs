﻿namespace Base2art.PlayN.Http.Owin.Security.Windows
{
	using System.Security.Principal;
    using Microsoft.Owin;

    public class WindowsAuthenticationMiddleware : OwinMiddleware
    {
        public WindowsAuthenticationMiddleware(OwinMiddleware next)
            : base(next)
        {
        }
        
        public async override System.Threading.Tasks.Task Invoke(IOwinContext context)
        {
            var windowsPrincipal = context.Environment["server.User"] as WindowsPrincipal;

            if (windowsPrincipal != null && context.Request.User != null)
            {
                context.Request.User = windowsPrincipal;
            }
            
            await this.Next.Invoke(context);
        }
    }
}
