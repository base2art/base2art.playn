﻿namespace Base2art.PlayN.Http.Owin
{
    using System;

    using Base2art.PlayN.Api.Config;

    using global::Owin;

    public static class AppBuilderExtender
    {
        public static string BaseDirectory(this IAppBuilder appBuilder)
        {
            var currentDomain = AppDomain.CurrentDomain;
            var baseDirectory = currentDomain.GetData(CommonSettings.RootDirectoryKey) as string;
            if (baseDirectory != null)
            {
                return baseDirectory;
            }

            return currentDomain.BaseDirectory;
        }
    }
}