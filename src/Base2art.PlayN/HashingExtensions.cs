﻿namespace Base2art.PlayN
{
    using System;
    using System.Security.Cryptography;
    using System.Text;

    public static class HashingExtensions
    {
        public static IHashResult Hash(this string value)
        {
            MD5 md5 = MD5.Create();
            byte[] inputBytes = Encoding.ASCII.GetBytes(value);
            return new Md5HashResult(md5.ComputeHash(inputBytes));
        }

        private class Md5HashResult : IHashResult
        {
            private readonly byte[] hashedBytes;

            public Md5HashResult(byte[] hashedBytes)
            {
                this.hashedBytes = hashedBytes;
            }

            public string AsString()
            {
                var hash = hashedBytes;
                // step 2, convert byte array to hex string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hash.Length; i++)
                {
                    sb.Append(hash[i].ToString("X2"));
                }

                return sb.ToString();
            }

            public Guid AsGuid()
            {
                var bytes = new byte[16];
                bytes[0] = this.hashedBytes[3];
                bytes[1] = this.hashedBytes[2];
                bytes[2] = this.hashedBytes[1];
                bytes[3] = this.hashedBytes[0];

                bytes[4] = this.hashedBytes[5];
                bytes[5] = this.hashedBytes[4];

                bytes[6] = this.hashedBytes[7];
                bytes[7] = this.hashedBytes[6];

                bytes[8] = this.hashedBytes[8];
                bytes[9] = this.hashedBytes[9];
                bytes[10] = this.hashedBytes[10];
                bytes[11] = this.hashedBytes[11];
                bytes[12] = this.hashedBytes[12];
                bytes[13] = this.hashedBytes[13];
                bytes[14] = this.hashedBytes[14];
                bytes[15] = this.hashedBytes[15];
                return new Guid(bytes);
            }
        }
    }
}
