namespace Base2art.PlayN.Criteria
{
    public class EnumCriteria<T>
        where T : struct 
    {
        public T? EqualTo { get; set; }

        public T? NotEqualTo { get; set; }
    }
}