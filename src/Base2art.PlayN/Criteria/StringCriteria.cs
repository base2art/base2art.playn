﻿namespace Base2art.PlayN.Criteria
{
    public class StringCriteria
    {
//        public bool IgnoreCase { get; set; }

        public string Contains { get; set; }

        public string EqualTo { get; set; }

        public string StartsWith { get; set; }

        public string EndsWith { get; set; }
    }
}