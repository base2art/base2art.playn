﻿namespace Base2art.PlayN.Api.Diagnostics
{
    public interface IApplicationLoggerFactory
    {
        ILogger Create(LogLevel logLevel);
    }
}
