﻿namespace Base2art.PlayN.Api.Diagnostics
{
    public interface ILogMessage
    {
        LogLevel Level { get; }

        string Message { get; }
    }
}