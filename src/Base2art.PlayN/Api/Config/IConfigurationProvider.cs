﻿namespace Base2art.PlayN.Api.Config
{
    public interface IConfigurationProvider
    {
        string GetValue(string key);
    }
}