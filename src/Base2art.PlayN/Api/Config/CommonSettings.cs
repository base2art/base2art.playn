﻿namespace Base2art.PlayN.Api.Config
{
    public static class CommonSettings
    {
        public const string SaltKey = "playn:salt";

        public const string AppModeKey = "playn:app-mode";

        public const string AppBuilderClassNameKey = "playn:app-builder-class-name";

        public const string RootDirectoryKey = "playn:root-directory";

        public const string LoggerFactoryClassNameKey = "playn:logger-factory-class-name";

        public const string LogLevelKey = "playn:log-level";
    }
}
