﻿namespace Base2art.PlayN.Api
{
    using Base2art.PlayN.Mvc;

    public class SimpleResult : IResult
    {
        public IContent Content { get; set; }

        public IResult As(string contentType)
        {
            var cntnt = this.Content as SimpleContent;

            if (cntnt != null)
            {
                cntnt.ContentType = contentType;
            }

            return this;
        }
    }
}