﻿namespace Base2art.PlayN.Api
{
    using System;
    using Base2art.ComponentModel;

    public class AggregatingInstantiatorInjector : IServiceLoaderInjector
    {
        private readonly IServiceLoaderInjector primaryInjector;

        private readonly IServiceLoaderInjector backingInjector;

        public AggregatingInstantiatorInjector(IServiceLoaderInjector primaryInjector, IServiceLoaderInjector backingInjector)
        {
            this.primaryInjector = primaryInjector;
            this.backingInjector = backingInjector;
        }
        
        public object Resolve(Type contractType)
        {
            return this.Resolve(contractType, false);
        }
        
        public T Resolve<T>(IClass<T> klazz)
        {
            return this.Resolve<T>(klazz, false);
        }
        
        public T Resolve<T>()
        {
            return this.Resolve<T>(false);
        }
        
        public object ResolveByNamed(Type contractType, string name)
        {
            return this.ResolveByNamed(contractType, name, false);
        }
        
        public T ResolveByNamed<T>(IClass<T> klazz, string name)
        {
            return this.ResolveByNamed<T>(klazz, name, false);
        }
        
        public T ResolveByNamed<T>(string name)
        {
            return this.ResolveByNamed<T>(name, false);
        }
        
        public object[] ResolveAll(Type contractType)
        {
            return this.ResolveAll(contractType, false);
        }
        
        public T[] ResolveAll<T>(IClass<T> klazz)
        {
            return this.ResolveAll<T>(klazz, false);
        }
        
        public T[] ResolveAll<T>()
        {
            return this.ResolveAll<T>(false);
        }
        
        public object Resolve(Type contractType, bool returnDefaultOnError)
        {
            var item = primaryInjector.Resolve(contractType, true);
            return item ?? backingInjector.Resolve(contractType, returnDefaultOnError);
        }
        
        public T Resolve<T>(IClass<T> klazz, bool returnDefaultOnError)
        {
            var item = primaryInjector.Resolve<T>(klazz, true);
            return this.Coalesce(item, () => backingInjector.Resolve<T>(klazz, returnDefaultOnError));
        }
        
        public T Resolve<T>(bool returnDefaultOnError)
        {
            var item = primaryInjector.Resolve<T>(true);
            return this.Coalesce(item, () => backingInjector.Resolve<T>(returnDefaultOnError));
        }
        
        public object ResolveByNamed(Type contractType, string name, bool returnDefaultOnError)
        {
            var item = primaryInjector.Resolve(contractType, true);
            return item ?? backingInjector.Resolve(contractType, returnDefaultOnError);
        }
        
        public T ResolveByNamed<T>(IClass<T> klazz, string name, bool returnDefaultOnError)
        {
            var item = primaryInjector.ResolveByNamed(klazz, name, true);
            return this.Coalesce(item, () => backingInjector.ResolveByNamed(klazz, name, returnDefaultOnError));
        }
        
        public T ResolveByNamed<T>(string name, bool returnDefaultOnError)
        {
            var item = primaryInjector.ResolveByNamed<T>(name, true);
            return this.Coalesce(item, () => backingInjector.ResolveByNamed<T>(name, returnDefaultOnError));
        }
        
        public object[] ResolveAll(Type contractType, bool returnDefaultOnError)
        {
            var item = primaryInjector.ResolveAll(contractType, true);
            return this.CoalesceArray(item, () => backingInjector.ResolveAll(contractType, returnDefaultOnError));
        }
        
        public T[] ResolveAll<T>(IClass<T> klazz, bool returnDefaultOnError)
        {
            var item = primaryInjector.ResolveAll<T>(klazz, true);
            return this.CoalesceArray(item, () => backingInjector.ResolveAll(klazz, returnDefaultOnError));
        }
        
        public T[] ResolveAll<T>(bool returnDefaultOnError)
        {
            var item = primaryInjector.ResolveAll<T>(true);
            return this.CoalesceArray(item, () => backingInjector.ResolveAll<T>(returnDefaultOnError));
        }
        
        public void VerifyAll()
        {
            throw new NotImplementedException();
        }

        private T Coalesce<T>(T item, Func<T> backup)
        {
            if (item != null)
            {
                return item;
            }
            
            return backup();
        }

        private T[] CoalesceArray<T>(T[] item, Func<T[]> backup)
        {
            if (item != null && item.Length > 0)
            {
                return item;
            }
            
            return backup();
        }
    }
}


/*
        public object Resolve(Type contractType, bool returnDefaultOnNotFound)
        {
            throw new NotImplementedException();
        }
        public T Resolve<T>(IClass<T> klazz, bool returnDefaultOnNotFound)
        {
            throw new NotImplementedException();
        }
        public T Resolve<T>(bool returnDefaultOnNotFound)
        {
            throw new NotImplementedException();
        }
        public object ResolveByNamed(Type contractType, string name, bool returnDefaultOnNotFound)
        {
            throw new NotImplementedException();
        }
        public T ResolveByNamed<T>(IClass<T> klazz, string name, bool returnDefaultOnNotFound)
        {
            throw new NotImplementedException();
        }
        public T ResolveByNamed<T>(string name, bool returnDefaultOnNotFound)
        {
            throw new NotImplementedException();
        }
        public object[] ResolveAll(Type contractType, bool returnDefaultOnNotFound)
        {
            throw new NotImplementedException();
        }
        public T[] ResolveAll<T>(IClass<T> klazz, bool returnDefaultOnNotFound)
        {
            throw new NotImplementedException();
        }
        public T[] ResolveAll<T>(bool returnDefaultOnNotFound)
        {
            throw new NotImplementedException();
        }
 */

/*
 
            
            public object Resolve(Type contractType)
            {
                var item = TryExecute(() => primaryInjector.Resolve(contractType), true);
                return item ?? primaryInjector.Resolve(contractType);
            }
            
            public T Resolve<T>(IClass<T> klazz)
            {
                var item = TryExecute(() => primaryInjector.Resolve<T>(klazz), true);
                return this.Coalesce(item, () => primaryInjector.Resolve<T>(klazz));
            }
            
            public T Resolve<T>()
            {
                var item = TryExecute(() => primaryInjector.Resolve<T>(), true);
                return this.Coalesce(item, () => primaryInjector.Resolve<T>());
            }
            
            public object ResolveByNamed(Type contractType, string name)
            {
                var item = TryExecute(() => primaryInjector.Resolve(contractType), true);
                return item ?? primaryInjector.Resolve(contractType);
            }
            
            public T ResolveByNamed<T>(IClass<T> klazz, string name)
            {
                var item = TryExecute(() => primaryInjector.ResolveByNamed(klazz, name), true);
                return this.Coalesce(item, () => primaryInjector.ResolveByNamed(klazz, name));
            }
            
            public T ResolveByNamed<T>(string name)
            {
                var item = TryExecute(() => primaryInjector.ResolveByNamed<T>(name), true);
                return this.Coalesce(item, () => primaryInjector.ResolveByNamed<T>(name));
            }
            
            public object[] ResolveAll(Type contractType)
            {
                var item = TryExecute(() => primaryInjector.ResolveAll(contractType), true);
                return item ?? primaryInjector.ResolveAll(contractType);
            }
            
            public T[] ResolveAll<T>(IClass<T> klazz)
            {
                var item = TryExecute(() => primaryInjector.ResolveAll<T>(klazz), true);
                return item ?? primaryInjector.ResolveAll(klazz);
            }
            
            public T[] ResolveAll<T>()
            {
                var item = TryExecute(() => primaryInjector.ResolveAll<T>(), true);
                return this.Coalesce(item, () => primaryInjector.ResolveAll<T>());
            }
 
 
 */
