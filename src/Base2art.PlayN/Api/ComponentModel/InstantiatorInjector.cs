﻿namespace Base2art.PlayN.Api
{
    using System;
    using Base2art.ComponentModel;
    using Base2art.PlayN.Api.Diagnostics;

    public class InstantiatorInjector : IServiceLoaderInjector
    {
        private readonly IApplication app;

        public InstantiatorInjector(IApplication app)
        {
            this.app = app;
        }
        
        public object Resolve(Type contractType, bool returnDefaultOnError)
        {
            return this.CreateInstanceByType(contractType, returnDefaultOnError);
        }

        public T Resolve<T>(IClass<T> klazz, bool returnDefaultOnError)
        {
            return this.CreateInstanceByClass(klazz, returnDefaultOnError);
        }

        public T Resolve<T>(bool returnDefaultOnError)
        {
            return this.CreateInstanceByClass(Class.GetClass<T>(), returnDefaultOnError);
        }

        public object ResolveByNamed(Type contractType, string name, bool returnDefaultOnError)
        {
            return this.CreateInstanceByType(contractType, returnDefaultOnError);
        }

        public T ResolveByNamed<T>(IClass<T> klazz, string name, bool returnDefaultOnError)
        {
            return this.CreateInstanceByClass(klazz, returnDefaultOnError);
        }

        public T ResolveByNamed<T>(string name, bool returnDefaultOnError)
        {
            return this.CreateInstanceByClass(Class.GetClass<T>(), returnDefaultOnError);
        }

        public object[] ResolveAll(Type contractType, bool returnDefaultOnError)
        {
            return new[] {
                this.CreateInstanceByType(contractType, returnDefaultOnError)
            };
        }

        public T[] ResolveAll<T>(IClass<T> klazz, bool returnDefaultOnError)
        {
            return new[] {
                this.CreateInstanceByClass(klazz, returnDefaultOnError)
            };
        }

        public T[] ResolveAll<T>(bool returnDefaultOnError)
        {
            return new[] {
                this.CreateInstanceByClass(Class.GetClass<T>(), returnDefaultOnError)
            };
        }

        public object Resolve(Type contractType)
        {
            return this.Resolve(contractType, false);
        }
        
        public T Resolve<T>(IClass<T> klazz)
        {
            return this.Resolve<T>(klazz, false);
        }
        
        public T Resolve<T>()
        {
            return this.Resolve<T>(false);
        }
        
        public object ResolveByNamed(Type contractType, string name)
        {
            return this.ResolveByNamed(contractType, name, false);
        }
        
        public T ResolveByNamed<T>(IClass<T> klazz, string name)
        {
            return this.ResolveByNamed<T>(klazz, name, false);
        }
        
        public T ResolveByNamed<T>(string name)
        {
            return this.ResolveByNamed<T>(name, false);
        }
        
        public object[] ResolveAll(Type contractType)
        {
            return this.ResolveAll(contractType, false);
        }
        
        public T[] ResolveAll<T>(IClass<T> klazz)
        {
            return this.ResolveAll<T>(klazz, false);
        }
        
        public T[] ResolveAll<T>()
        {
            return this.ResolveAll<T>(false);
        }
        
        public void VerifyAll()
        {
        }

        private T CreateInstanceByClass<T>(IClass<T> type, bool returnDefaultOnError)
            //            where T : class
        {
            var t = type.Type;
            return (T)this.CreateInstanceByType(type.Type, returnDefaultOnError);
        }

        private object CreateInstanceByType(Type type, bool returnDefaultOnError)
        {
            try
            {
                return Activator.CreateInstance(type);
                
            } catch (Exception ex)
            {
                if (!type.IsAssignableFrom(typeof(IApplicationLoggerFactory)))
                {
                    this.app.ApplicationLogger.Log(ex.ToString(), LogLevels.ApplicationDebug);
                }
                
                if (returnDefaultOnError)
                {
                    return null;
                }
                
                throw;
            }
        }
    }
}




/*
 
            
            public object Resolve(Type contractType)
            {
                var item = TryExecute(() => primaryInjector.Resolve(contractType), true);
                return item ?? primaryInjector.Resolve(contractType);
            }
            
            public T Resolve<T>(IClass<T> klazz)
            {
                var item = TryExecute(() => primaryInjector.Resolve<T>(klazz), true);
                return this.Coalesce(item, () => primaryInjector.Resolve<T>(klazz));
            }
            
            public T Resolve<T>()
            {
                var item = TryExecute(() => primaryInjector.Resolve<T>(), true);
                return this.Coalesce(item, () => primaryInjector.Resolve<T>());
            }
            
            public object ResolveByNamed(Type contractType, string name)
            {
                var item = TryExecute(() => primaryInjector.Resolve(contractType), true);
                return item ?? primaryInjector.Resolve(contractType);
            }
            
            public T ResolveByNamed<T>(IClass<T> klazz, string name)
            {
                var item = TryExecute(() => primaryInjector.ResolveByNamed(klazz, name), true);
                return this.Coalesce(item, () => primaryInjector.ResolveByNamed(klazz, name));
            }
            
            public T ResolveByNamed<T>(string name)
            {
                var item = TryExecute(() => primaryInjector.ResolveByNamed<T>(name), true);
                return this.Coalesce(item, () => primaryInjector.ResolveByNamed<T>(name));
            }
            
            public object[] ResolveAll(Type contractType)
            {
                var item = TryExecute(() => primaryInjector.ResolveAll(contractType), true);
                return item ?? primaryInjector.ResolveAll(contractType);
            }
            
            public T[] ResolveAll<T>(IClass<T> klazz)
            {
                var item = TryExecute(() => primaryInjector.ResolveAll<T>(klazz), true);
                return item ?? primaryInjector.ResolveAll(klazz);
            }
            
            public T[] ResolveAll<T>()
            {
                var item = TryExecute(() => primaryInjector.ResolveAll<T>(), true);
                return this.Coalesce(item, () => primaryInjector.ResolveAll<T>());
            }
 
 
 */
