﻿namespace Base2art.PlayN.Api
{
    
	using Base2art.PlayN.Api.Diagnostics;
    using Base2art.PlayN.Mvc;

    using Base2art.PlayN.Http;

    public interface IApplication
    {
        string RootDirectory { get; }
        
        ILogger ApplicationLogger { get; }

        T CreateInstance<T>(IClass<T> createInstanceClass, bool returnNullOnErrorOrNotFound)
            where T : class;

        T[] CreateInstances<T>(IClass<T> createInstanceClass, bool returnNullOnErrorOrNotFound)
                where T : class;

        IResult OnControllerNotFound(IHttpContext httpContext);

        IRouter CreateRouter();

        string ConfigurationValue(string key);

        ApplicationMode Mode { get; }
    }
}