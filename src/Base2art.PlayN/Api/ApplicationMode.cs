﻿namespace Base2art.PlayN.Api
{
    public enum ApplicationMode
    {
        Prod = 0,
        Test = 1,
        Dev = 2 
    }
}
