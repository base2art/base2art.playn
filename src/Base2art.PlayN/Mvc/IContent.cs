﻿namespace Base2art.PlayN.Mvc
{
    public interface IContent
    {
        byte[] Body { get; }
        
        string BodyAsString { get; }

        string ContentType { get; }
    }
}