﻿namespace Base2art.PlayN.Mvc
{
    using Base2art.PlayN.Api;
    
    public class NullRenderingController : IRenderingController
    {
        IResult IRenderingController.Execute(Base2art.PlayN.Http.IHttpContext httpContext, System.Collections.Generic.List<Base2art.PlayN.Api.PositionedResult> childResults)
        {
            return new SimpleResult{ Content = new SimpleContent{ BodyContent = string.Empty } };
        }

        IPositionedRenderingController[] IRenderingController.RenderingControllers
        {
            get { return new IPositionedRenderingController[0]; }
        }

        INonRenderingController[] IRenderingController.NonRenderingControllers
        {
            get { return new INonRenderingController[0]; }
        }
    }
    
    
}
