﻿namespace Base2art.PlayN.Mvc
{
	public class NullNonRenderingController : INonRenderingController
	{
		void INonRenderingController.Execute(Base2art.PlayN.Http.IHttpContext httpContext)
		{
		}

		INonRenderingController[] INonRenderingController.NonRenderingControllers
		{
			get
			{
				return new INonRenderingController[0];
			}
		}
	}
}


