﻿namespace Base2art.PlayN.Linq
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;

    using Base2art.PlayN.Criteria;

    public static class QueryableExtensions
    {
        public static IQueryable<T> Filter<T>(
            this IQueryable<T> dataset,
            Expression<Func<T, string>> func,
            StringCriteria criteria)
        {
            if (criteria == null)
            {
                return dataset;
            }

            if (!string.IsNullOrWhiteSpace(criteria.Contains))
            {
                dataset = dataset.Where(CreateExpressionTree(func, x => x.Contains(criteria.Contains)));
            }

            if (!string.IsNullOrWhiteSpace(criteria.StartsWith))
            {
                dataset = dataset.Where(CreateExpressionTree(func, x => x.StartsWith(criteria.StartsWith)));
            }

            if (!string.IsNullOrWhiteSpace(criteria.EndsWith))
            {
                dataset = dataset.Where(CreateExpressionTree(func, x => x.EndsWith(criteria.EndsWith)));
            }

            if (!string.IsNullOrWhiteSpace(criteria.EqualTo))
            {
                dataset = dataset.Where(CreateExpressionTree(func, x => x == criteria.EqualTo));
            }

            return dataset;
        }

        public static IQueryable<T> FilterEnum<T, TEnum>(
            this IQueryable<T> dataset,
            Expression<Func<T, TEnum>> func,
            //            Expression<Func<TEnum, TEnum, bool>> comparer,
            EnumCriteria<TEnum> criteria)
            where TEnum : struct, IConvertible
        {
            if (criteria == null)
            {
                return dataset;
            }


            if (criteria.EqualTo.HasValue)
            {
                dataset = dataset.Where(CreateExpressionTreeForStruct(func, Expression.Equal, criteria.EqualTo.Value));
            }

            if (criteria.NotEqualTo.HasValue)
            {
                dataset = dataset.Where(CreateExpressionTreeForStruct(func, Expression.NotEqual, criteria.NotEqualTo.Value));
            }

            return dataset;
        }

        // http://stackoverflow.com/questions/10317669/passing-an-expression-tree-as-a-parameter-to-another-expression-tree-in-entityda
        private static Expression<Func<T, bool>> CreateExpressionTree<T>(
            Expression<Func<T, string>> propGetter,
            Expression<Func<string, bool>> func)
        {
            var remover = new ParameterReplaceVisitor(propGetter.Body);
            var bb = remover.Visit(func.Body);
            return Expression.Lambda<Func<T, bool>>(
                bb,
                propGetter.Parameters);
        }

        // http://stackoverflow.com/questions/10317669/passing-an-expression-tree-as-a-parameter-to-another-expression-tree-in-entityda
        private static Expression<Func<T, bool>> CreateExpressionTreeForStruct<T, TStruct>(
            Expression<Func<T, TStruct>> propGetter,
            Func<Expression, Expression, BinaryExpression> exprFunc,
            TStruct value)
            where TStruct : struct
        {
            return Expression.Lambda<Func<T, bool>>(
                exprFunc(propGetter.Body, Expression.Constant(value)),
                propGetter.Parameters);
        }

        private class ParameterReplaceVisitor : ExpressionVisitor
        {
            private readonly Expression replacement;

            public ParameterReplaceVisitor(Expression replacement)
            {
                this.replacement = replacement;
            }

            protected override Expression VisitParameter(ParameterExpression node)
            {
                return this.replacement;
            }
        }
    }
}
/*
 
                var expressionTree = criteria.IgnoreCase
                    ? CreateExpressionTree(func, x => x.ToUpperInvariant().Contains(criteria.Contains.ToUpperInvariant()))
                    : CreateExpressionTree(func, x => x.Contains(criteria.Contains));
 */