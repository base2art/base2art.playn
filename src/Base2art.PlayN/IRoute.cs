﻿namespace Base2art.PlayN
{
    public interface IRoute
    {
        string Explode();

        string ToString();
    }
}