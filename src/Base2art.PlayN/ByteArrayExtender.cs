﻿namespace Base2art.PlayN
{
    public static class ByteArrayExtender
    {
        public static string AsString(this byte[] arr)
        {
            if (arr == null || arr.Length == 0)
            {
                return string.Empty;
            }
            
            return System.Text.Encoding.Default.GetString(arr);
        }
    }
}
