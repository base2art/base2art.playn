﻿namespace Base2art.PlayN
{
    using System;

    public interface IHashResult
    {
        string AsString();

        Guid AsGuid();
    }
}