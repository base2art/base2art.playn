﻿namespace Base2art.PlayN.Http
{
    public interface IHttpRequestBody
    {
        bool IsMaxSizeExceeded { get; }

        byte[] AsRaw();

        string AsText();
    }
}