﻿namespace Base2art.PlayN.Http
{
    using System.Collections.Generic;

    using Base2art.Collections;

    public interface IHttpMultipartData
    {
        IReadOnlyMultiMap<string, string> AsFormUrlEncoded();

        IKeyedCollection<string, IHttpFilePart> Files();
    }
}