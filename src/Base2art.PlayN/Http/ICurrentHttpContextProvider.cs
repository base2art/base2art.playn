﻿namespace Base2art.PlayN.Http
{
    public interface ICurrentHttpContextProvider
    {
        IHttpContext Current { get; }
    }
}
