﻿namespace Base2art.PlayN.Http
{
    public interface IHttpFilePart
    {
        string ContentType { get; }
        
        string Key { get; }
        
        string FileName { get; }
        
        System.IO.FileInfo File { get; }
    }
}