﻿namespace Base2art.PlayN.Http
{
    using Base2art.Collections;

    public interface IHttpSession : IMap<string, string>
    {
    }
}