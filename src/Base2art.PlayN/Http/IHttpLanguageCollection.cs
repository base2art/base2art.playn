﻿namespace Base2art.PlayN.Http
{
    using System.Globalization;

    using Base2art.Collections.Generic;

    public interface IHttpLanguageCollection : IReadOnlyKeyedCollection<string, CultureInfo>
    {
    }
}