﻿namespace Base2art.PlayN.Http
{
    using Base2art.Collections.Generic;

    public interface IHttpResponseHeaderCollection : IMap<string, string>
    {
    }
}