﻿namespace Base2art.PlayN.Http
{
    public interface IHttpCookie
    {
        string Name { get; }

        string Value { get; }
    }
}

//        string Domain { get; }
//
//        bool HttpOnly { get; }
//
//        int MaxAge { get; }

//        string Path { get; }
//
//        bool Secure { get; }