﻿namespace Base2art.PlayN.Http
{
    using System.Security.Principal;

	public interface IHttpUserLookup
	{
		IHttpUser FindUser(IPrincipal principal);
	}
}

