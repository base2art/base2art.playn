﻿namespace Base2art.PlayN.Http
{
    using Base2art.PlayN.Api;
    using Base2art.PlayN.Api.Diagnostics;

    public interface IHttpContext
    {
        IApplication ApplicationInstance { get; }

        ILogger Logger { get; }

        IHttpFlash Flash { get; }
        
        IHttpSession Session { get; }
 
        IHttpResponse Response { get; }

        IHttpRequest Request { get; }
    }
}