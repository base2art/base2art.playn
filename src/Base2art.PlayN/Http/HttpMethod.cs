﻿namespace Base2art.PlayN.Http
{
    public enum HttpMethod
    {
        Get,
        Delete,
        Head,
        Options,
        Post,
        Put,
        Trace
    }
}
