﻿namespace Base2art.PlayN.Api.Routing.Expressive
{
    public interface IExpressiveRouteManager : IExpressiveReverseRouter, IExpressiveRouter
    {
    }
}