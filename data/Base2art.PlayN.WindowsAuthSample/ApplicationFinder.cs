﻿namespace Base2art.PlayN.WindowsAuthSample
{
    using Base2art.PlayN.Api;
    using Base2art.PlayN.Api.Config;
    using Base2art.PlayN.Http.Owin;

    public class ApplicationBuilder : IApplicationBuilder
    {
        public IApplication BuildApplication(ApplicationMode mode, string rootDirectory, IConfigurationProvider configProvider)
        {
            return new Application(mode, rootDirectory, configProvider);
        }
    }
}