﻿

namespace Base2art.PlayN.WindowsAuthSample
{
    using System.Collections.Generic;
    using System.Text.RegularExpressions;

    using Base2art.ComponentModel;
    using Base2art.PlayN.Api;
    using Base2art.PlayN.Api.Config;
    using Base2art.PlayN.Api.Routing.Expressive;
    using Base2art.PlayN.Corporate.Controllers;
    using Base2art.PlayN.Http;
    using Base2art.PlayN.Http.Owin;

    public class Application : Api.Application, IApplicationExtender
    {
        private readonly IServiceLoaderInjector loader;

        public Application(ApplicationMode mode, string rootDirectory, IConfigurationProvider configurationProvider)
            : base(mode, rootDirectory, configurationProvider)
        {
            this.loader = this.CreateServiceLoaderInjector();
        }

        public Owin.IAppBuilder Configure(Owin.IAppBuilder app)
        {
            return app;
        }
        
        protected override T CreateItemInstance<T>(IClass<T> type, bool returnNullOnErrorOrNotFound)
        {
            try
            {
                return this.loader.Resolve(type);
            }
            catch (KeyNotFoundException)
            {
            }
            
            return base.CreateItemInstance<T>(type, returnNullOnErrorOrNotFound);
        }

        protected override IRouter CreateRouter()
        {
            return this.CreateInstance(Class.GetClass<IRouter>(), true);
        }

        protected virtual IExpressiveRouteManager CreateRouterTyped(
            IServiceLoaderInjector injector)
        {
            var expressiveRouter = new MappedLocalHostExpressiveRouter("www.base2art.com", injector.Resolve<ICurrentHttpContextProvider>());
            
            

            expressiveRouter.Register("/user")
                .OnDomain("www.scottyoungblut.com")
                .WithMethod(HttpMethod.Get)
                .To<HomePage>()
                .Method((a, b, c) => a.Execute(b, c));

            expressiveRouter.Register("/session-set")
                .OnDomain("www.scottyoungblut.com")
                .WithMethod(HttpMethod.Get)
                .To<HomePage>()
                .Method((a, b, c) => a.AboutUs(b, c));

            expressiveRouter.RegisterRoute<HomePage>(HttpMethod.Get, "www.base2art.com", "/");
            expressiveRouter.RegisterRoute<HomePage>(HttpMethod.Get, "www.base2art.com", "/page-2", (a, b, c) => a.Execute(b, c, 2));
            expressiveRouter.RegisterRoute<HomePage>(HttpMethod.Get, "www.base2art.com", "/about-us", (a, b, c) => a.AboutUs(b, c));


            expressiveRouter.Register("/dynamic/override-static")
                .OnDomain("www.base2art.com")
                .WithMethod(HttpMethod.Get)
                .To<HomePage>()
                .Method((a, b, c) => a.DynamicPage(b, c, "override"));

            expressiveRouter.Register(new Regex("/dynamic/(?<displayText>.*)"), "/dyanmic/{displayText}")
                .OnDomain("www.base2art.com")
                .WithMethod(HttpMethod.Get)
                .To<HomePage>()
                .Method<string>((a, b, c, displayText) => a.DynamicPage(b, c, displayText));

            expressiveRouter.Register(new Regex("^/en/int-test/(?<number>\\d+)$"), "/en/int-test/{number}")
                .OnDomain("www.base2art.com")
                .WithMethod(HttpMethod.Get)
                .To<HomePage>()
                .Method<int>((a, b, c, number) => a.NumberPrinter(b, c, "start", number, "end"));

            expressiveRouter.Register(new Regex("/spanish/int-test/(?<number>\\d+)"), "/spanish/int-test/{number}")
                .OnDomain("www.base2art.com")
                .WithMethod(HttpMethod.Get)
                .To<HomePage>()
                .Method<int>((a, b, c, number) => a.NumberPrinter(b, c, "comienzo", number, "fin"));
            return expressiveRouter;
        }

        private IBindableAndSealableServiceLoaderInjector CreateServiceLoaderInjector()
        {
            var editableLoader = ServiceLoader.CreateLoader();

            editableLoader.Bind<IExpressiveRouteManager>()
                .As(ServiceLoaderBindingType.Singleton)
                .To(this.CreateRouterTyped);

            editableLoader.Bind<IRouter>()
                .As(ServiceLoaderBindingType.Singleton)
                .To(x => x.Resolve<IExpressiveRouteManager>());

            editableLoader.Bind<HomePage>()
                .As(ServiceLoaderBindingType.Singleton)
                .To(x => new HomePage(x.Resolve<ICurrentHttpContextProvider>(), x.Resolve<IExpressiveRouteManager>()));

            editableLoader.Bind<IApplicationExtender>()
                .As(ServiceLoaderBindingType.Singleton)
                .To(x => this);

            editableLoader.Bind<ICurrentHttpContextProvider>()
                .As(ServiceLoaderBindingType.Instance)
                .To(x => new CurrentHttpContextProvider());

            return editableLoader;
        }
//
//        public Owin.IAppBuilder Configure(Owin.IAppBuilder app)
//        {
//            return app.UseWindowsAuthentication();
//        }
    }
}