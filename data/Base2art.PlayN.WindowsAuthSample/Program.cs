﻿
namespace Base2art.PlayN.WindowsAuthSample
{
	using System;
    using Base2art.PlayN.Http.Owin;
	using Microsoft.Owin.Hosting;
    
    class Program
    {
        static void Main(string[] args)
        {
            using (WebApp.Start<Startup>("http://localhost:9000"))
            {
                Console.WriteLine("Press Enter to quit.");
                Console.ReadKey();
            }
        }
    }
}
