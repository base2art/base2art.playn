﻿namespace Base2art.PlayN.Samples.Session
{
    using System.Collections.Generic;
    using System.Linq;

    using Base2art.PlayN.Api;
    using Base2art.PlayN.Http;
    using Base2art.PlayN.Mvc;

    public class SessionReaderController : SimpleRenderingController
    {
        protected override IResult ExecuteMain(IHttpContext httpContext, List<PositionedResult> childResults)
        {
            IHttpQueryString httpQueryString = httpContext.Request.QueryString;
            if (httpQueryString.Contains("key-name"))
            {
                string keyName = httpQueryString["key-name"].FirstOrDefault() ?? "value";
                return new SimpleResult { Content = new SimpleContent { BodyContent = httpContext.Session[keyName] } };
            }

            return new SimpleResult { Content = new SimpleContent { BodyContent = "Key Not Found" } };
        }
    }
}
