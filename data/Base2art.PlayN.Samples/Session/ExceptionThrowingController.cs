﻿namespace Base2art.PlayN.Samples.Session
{
    using System;
    using System.Collections.Generic;

    using Base2art.PlayN.Api;
    using Base2art.PlayN.Http;
    using Base2art.PlayN.Mvc;

    public class ExceptionThrowingController : SimpleRenderingController
    {
        protected override IResult ExecuteMain(IHttpContext httpContext, List<PositionedResult> childResults)
        {
            throw new NotImplementedException("Oh Boy!", new InvalidOperationException("Op Ex"));
        }
    }
}