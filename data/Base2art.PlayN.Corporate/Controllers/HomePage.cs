﻿namespace Base2art.PlayN.Corporate.Controllers
{
    using System.Collections.Generic;
    using System.Text;

    using Base2art.PlayN.Api;
    using Base2art.PlayN.Api.Routing.Expressive;
    using Base2art.PlayN.Http;
    using Base2art.PlayN.Mvc;

    public class HomePage : SimpleRenderingController
    {
        private readonly IExpressiveRouteManager router;

        public HomePage(IExpressiveRouteManager router)
        {
            this.router = router;
        }

        protected override IResult ExecuteMain(IHttpContext httpContext, List<PositionedResult> childResults)
        {
            return httpContext.Ok(new SimpleContent { BodyContent = "Home Page" });
        }

        public IResult Execute(IHttpContext httpContext, List<PositionedResult> childResults, int i)
        {
            return httpContext.Ok(new SimpleContent { BodyContent = "Home Page with Id" + i });
        }

        public IResult AboutUs(IHttpContext httpContext, List<PositionedResult> positionedResults)
        {
            return httpContext.Ok(new SimpleContent { BodyContent = "About Us!" });
        }

        public IResult DynamicPage(IHttpContext httpContext, List<PositionedResult> positionedResults, string displayText)
        {
            return httpContext.Ok(new SimpleContent { BodyContent = displayText });
        }

        public IResult NumberPrinter(
            IHttpContext httpContext,
            List<PositionedResult> positionedResults,
            string start,
            int someTextAsInt,
            string end)
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendFormat("{0} -> {1} <- {2}", start, someTextAsInt, end);
            sb.AppendLine();
            sb.AppendLine();

            sb.AppendFormat("this.router.FindRoute<HomePage>(): {0}", this.router.FindRoute<HomePage>());
            sb.AppendLine();
            sb.AppendLine();

            sb.AppendFormat("this.router.FindRouteWith<HomePage>(2): {0}", this.router.FindRouteWith<HomePage>(2));
            sb.AppendLine();
            sb.AppendLine();

            sb.AppendLine("# Can I use Expressions to simplify this and get compile time checking...");
            sb.AppendFormat("this.router.FindNamedRoute<HomePage>('AboutUs'): {0}", this.router.FindNamedRoute<HomePage>("AboutUs"));
            sb.AppendLine();
            sb.AppendLine();

            sb.AppendFormat(
                "this.router.FindNamedRouteWith<HomePage>('DynamicPage', 'override'): {0}",
                this.router.FindNamedRouteWith<HomePage>("DynamicPage", "override"));
            sb.AppendLine();
            sb.AppendLine();


            sb.AppendFormat(
                "this.router.For<HomePage>().With((x, y, z) => x.DynamicPage(y,z, 'override-dynamic'))): {0}",
                this.router.For<HomePage>().With((x, y, z) => x.DynamicPage(y, z, "override-dynamic")));
            sb.AppendLine();
            sb.AppendLine();

            return httpContext.Ok(new SimpleContent { BodyContent = sb.ToString() });
        }
    }
}