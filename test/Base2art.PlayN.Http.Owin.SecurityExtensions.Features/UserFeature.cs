﻿namespace Base2art.PlayN.Http.Owin.Security
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Security.Claims;
    using System.Security.Principal;
    using Base2art.Collections.Generic;
    using Base2art.ComponentModel;
    using Base2art.Net;
    using Base2art.PlayN.Api;
    using Base2art.PlayN.Api.Config;
    using Base2art.PlayN.Api.Diagnostics;
    using Base2art.PlayN.Http.Owin.Security;
	using Base2art.PlayN.Security;
    using FluentAssertions;
    using Microsoft.Owin;
    using Microsoft.Owin.Testing;
    using NUnit.Framework;

    [TestFixture]
    public class UserFeature : AppBaseFeature, IApplicationExtender
    {
        [Test]
        public void ShouldWorkWithNoUser()
        {
            //?user=Scott&group=Youngbluts&group=Developers
            OwinContext context = OwinExtender.CreateRequestForPath("/print-user");
            
            context.Request.User = null;
            
            var result = context.ProcessRequest(this.Manager, new ClaimsIdentityHttpUserLookup(), this.CommonSalt, new NullLogger());
            result.Content.Body.AsString().Should().Be("[]False");
        }
        
        [Test]
        public async void ShouldGetUser()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<IApplicationExtender>().To(x => this);
            loader.Bind<IHttpUserLookup>().To(x => new ClaimsIdentityHttpUserLookup());
            loader.Bind<IRouter>().To(x => CreateRouter());
            
            using (Using.TransactionallyAssignTo(loader, () => ServiceLoader.PrimaryLoader))
            {
                using (var server = TestServer.Create<TestStartup>())
                {
                    HttpRequestMessage message = new HttpRequestMessage(
                        HttpMethod.Get,
                        new Uri("http://localhost/print-user"));
                    //?username=scott
                    
                    
                    var cookieJar = new KeyValuePairCookieJar(new SecureCookieJarSettings
                                                              {
                                                                  ApplicationSaltSettings = new TestStartup()
                                                                      .CreateTestConfig("")
                                                                      .GetValue(CommonSettings.SaltKey)
                                                              });
                    
                    var map = new MultiMap<string, string>();
                    map.Add(TokenAuthenticationMiddleware.NameKey, "scott");
                    map.Add(TokenAuthenticationMiddleware.GroupKey, "Youngblut");
                    map.Add(TokenAuthenticationMiddleware.GroupKey, "Developer");
                    cookieJar.SetSecureCookieValues("user", map);
                    string value = cookieJar.GetCookieValue("user");
                    
                    message.Headers.Add("Cookie", "user=" + value);
                    
                    HttpResponseMessage response = await server.HttpClient.SendAsync(message);
                    (await response.Content.ReadAsStringAsync()).Should().Be("scott[Youngblut,Developer]True");
                }
            }
        }
        
        global::Owin.IAppBuilder IApplicationExtender.Configure(global::Owin.IAppBuilder app)
        {
            return app.UseTokenAuthentication(new TokenAuthenticationOptions());
        }
    }
}

