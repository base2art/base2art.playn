﻿namespace Base2art.PlayN.Http.Owin.Security.Fixtures
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.Owin;

    public class ClaimsPrincipalSetupMiddleware : OwinMiddleware
    {
        private readonly ClaimsPrincipalSetupOptions options;

        public ClaimsPrincipalSetupMiddleware(OwinMiddleware next) : base(next)
        {
        }

        public ClaimsPrincipalSetupMiddleware(OwinMiddleware next, ClaimsPrincipalSetupOptions options) : base(next)
        {
            this.options = options;
        }
        
        public async override Task Invoke(IOwinContext context)
        {
            var item = this.options.Principal;
            if (item != null)
            {
                context.Request.User = item();
            }
            
            await Next.Invoke(context);
        }
    }
    
    
}




