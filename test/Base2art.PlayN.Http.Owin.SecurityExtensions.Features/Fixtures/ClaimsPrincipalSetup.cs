﻿namespace Base2art.PlayN.Http.Owin.Security.Fixtures
{
    using System;
    using System.Security.Claims;
    using global::Owin;

    public class ClaimsPrincipalSetup : IApplicationExtender
    {
        public static ClaimsPrincipal Principal { get; set; }
        
        global::Owin.IAppBuilder IApplicationExtender.Configure(global::Owin.IAppBuilder app)
        {
            return app.Use<ClaimsPrincipalSetupMiddleware>(new ClaimsPrincipalSetupOptions{Principal = (() => Principal)});
        }
    }

    
}


