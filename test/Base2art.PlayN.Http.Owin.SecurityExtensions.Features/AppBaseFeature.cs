﻿namespace Base2art.PlayN.Http.Owin.Security
{
    using System;

    using Base2art.PlayN.Api;
    using Base2art.PlayN.Api.Routing.Functional;

	using Base2art.PlayN.Samples.Session;
    using NUnit.Framework;

    public class AppBaseFeature
    {
        private ControllerExecutionManager manager;

        public string CommonSalt
        {
            get
            {
                return "salt";
            }
        }


        public ControllerExecutionManager Manager
        {
            get
            {
                if (this.manager == null)
                {
                    this.manager = CreateManager(this.AppMode);
                }

                return this.manager;
            }
        }

        protected void ClearManager()
        {
            this.manager = null;
        }

        [SetUp]
        public void SetUpManager()
        {
            this.ClearManager();
        }

        protected virtual ApplicationMode AppMode
        {
            get
            {
                return ApplicationMode.Test;
            }
        }

        private static ControllerExecutionManager CreateManager(ApplicationMode applicationMode)
        {
            IApplication app = new Application(applicationMode, Environment.CurrentDirectory, null);
            var m = new ControllerExecutionManager(
                app,
                CreateRouter());
            return m;
        }

		protected static IRouter CreateRouter()
		{
			return new FunctionalRouter(new[] {
				new FunctionalRenderingControllerSearchDelegate(MapPath)
			}, null);
		}
		
        private static Type MapPath(IHttpRequest arg)
        {
            if (arg.Path == "/print-user")
            {
                return typeof(UserReaderController);
            }

            return null;
        }
    }
}