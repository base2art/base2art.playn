﻿namespace Base2art.PlayN.Http.Owin.Security
{
    using System;
    using System.Linq;
    using System.Net.Http;
    using System.Security.Claims;
    using System.Security.Principal;
    using Base2art.Collections.Generic;
    using Base2art.ComponentModel;
    using Base2art.Net;
    using Base2art.PlayN.Api;
    using Base2art.PlayN.Api.Config;
	using Base2art.PlayN.Security;
    using FluentAssertions;
    using Microsoft.Owin.Testing;
    using NUnit.Framework;
    using Base2art.PlayN.Http.Owin.Security.Fixtures;

    [TestFixture]
    public class WindowsAuthMockFeature : AppBaseFeature, IApplicationExtender
    {
        [Test]
        public async void ShouldAuthenticateCorrectlyAndSaveDataToCookie()
        {
            
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<IApplicationExtender>().To(x => new ClaimsPrincipalSetup());
            loader.Bind<IApplicationExtender>().To(x => this);
            loader.Bind<IHttpUserLookup>().To(x => new ClaimsIdentityHttpUserLookup());
            loader.Bind<IRouter>().To(x => CreateRouter());
            
            using (Using.TransactionallyAssignTo(loader, () => ServiceLoader.PrimaryLoader))
            {
                using (var server = TestServer.Create<TestStartup>())
                {
                    var cookieValue = "";
                    using (CreatePrincipal().Assign().TransactionallyTo(() => ClaimsPrincipalSetup.Principal))
                    {
                        HttpRequestMessage message1 = new HttpRequestMessage(
                            HttpMethod.Get,
                            new Uri("http://localhost/print-user"));
                        
                        HttpResponseMessage response1 = await server.HttpClient.SendAsync(message1);
                        (await response1.Content.ReadAsStringAsync()).Should().Be("scott[Tester,developer]True");
                        var cookies = response1.Headers.GetValues("Set-Cookie");
                        var str = cookies.First(x => x.StartsWith("user=")).TakeWhile(x=>x!= ';').ToArray();
                        cookieValue = new string(str);
                    }
                    
                    
                    HttpRequestMessage messageNoCookie = new HttpRequestMessage(
                        HttpMethod.Get,
                        new Uri("http://localhost/print-user"));
                    
                    HttpResponseMessage responseNoCookie = await server.HttpClient.SendAsync(messageNoCookie);
                    (await responseNoCookie.Content.ReadAsStringAsync()).Should().Be("[]False");
                    
                    
                    HttpRequestMessage messageWithCookie = new HttpRequestMessage(
                        HttpMethod.Get,
                        new Uri("http://localhost/print-user"));
                    
                    messageWithCookie.Headers.Add("Cookie", cookieValue );
                    HttpResponseMessage responseWithCookie = await server.HttpClient.SendAsync(messageWithCookie);
                    (await responseWithCookie.Content.ReadAsStringAsync()).Should().Be("scott[Tester,developer]True");
                }
            }
        }
        
        [Test]
        public async void ShouldMergeWindowsAuthAndFormsAuth()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<IApplicationExtender>().To(x => new ClaimsPrincipalSetup());
            loader.Bind<IApplicationExtender>().To(x => this);
            loader.Bind<IHttpUserLookup>().To(x => new ClaimsIdentityHttpUserLookup());
            loader.Bind<IRouter>().To(x => CreateRouter());
            
            using (Using.TransactionallyAssignTo(loader, () => ServiceLoader.PrimaryLoader))
            {
                using (var server = TestServer.Create<TestStartup>())
                {
                    var cookieValue = "";
                    using (CreatePrincipal("scott", "Tester").Assign().TransactionallyTo(() => ClaimsPrincipalSetup.Principal))
                    {
                        HttpRequestMessage message1 = new HttpRequestMessage(
                            HttpMethod.Get,
                            new Uri("http://localhost/print-user"));
                        
                        HttpResponseMessage response1 = await server.HttpClient.SendAsync(message1);
                        (await response1.Content.ReadAsStringAsync()).Should().Be("scott[Tester]True");
                        var cookies = response1.Headers.GetValues("Set-Cookie");
                        var str = cookies.First(x => x.StartsWith("user=")).TakeWhile(x => x!= ';').ToArray();
                        cookieValue = new string(str);
                    }
                    
                    using (CreatePrincipal("scott", "developer").Assign().TransactionallyTo(() => ClaimsPrincipalSetup.Principal))
                    {
                        HttpRequestMessage messageWithCookie = new HttpRequestMessage(
                            HttpMethod.Get,
                            new Uri("http://localhost/print-user"));
                        
                        messageWithCookie.Headers.Add("Cookie", cookieValue );
                        HttpResponseMessage responseWithCookie = await server.HttpClient.SendAsync(messageWithCookie);
                        (await responseWithCookie.Content.ReadAsStringAsync()).Should().Be("scott[developer,Tester]True");
                    }
                }
            }
        }
        
        
        [Test]
        public async void ShouldPreferWindowsAuthOverFormsAuth()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<IApplicationExtender>().To(x => new ClaimsPrincipalSetup());
            loader.Bind<IApplicationExtender>().To(x => this);
            loader.Bind<IHttpUserLookup>().To(x => new ClaimsIdentityHttpUserLookup());
            loader.Bind<IRouter>().To(x => CreateRouter());
            
            using (Using.TransactionallyAssignTo(loader, () => ServiceLoader.PrimaryLoader))
            {
                using (var server = TestServer.Create<TestStartup>())
                {
                    var cookieValue = "";
                    using (CreatePrincipal("scott", "Tester").Assign().TransactionallyTo(() => ClaimsPrincipalSetup.Principal))
                    {
                        HttpRequestMessage message1 = new HttpRequestMessage(
                            HttpMethod.Get,
                            new Uri("http://localhost/print-user"));
                        
                        HttpResponseMessage response1 = await server.HttpClient.SendAsync(message1);
                        (await response1.Content.ReadAsStringAsync()).Should().Be("scott[Tester]True");
                        var cookies = response1.Headers.GetValues("Set-Cookie");
                        var str = cookies.First(x => x.StartsWith("user=")).TakeWhile(x=>x!= ';').ToArray();
                        cookieValue = new string(str);
                    }
                    
                    
                    using (CreatePrincipal("matt", "developer").Assign().TransactionallyTo(() => ClaimsPrincipalSetup.Principal))
                    {
                        HttpRequestMessage messageWithCookie = new HttpRequestMessage(
                            HttpMethod.Get,
                            new Uri("http://localhost/print-user"));
                        
                        messageWithCookie.Headers.Add("Cookie", cookieValue);
                        HttpResponseMessage responseWithCookie = await server.HttpClient.SendAsync(messageWithCookie);
                        (await responseWithCookie.Content.ReadAsStringAsync()).Should().Be("matt[developer]True");
                    }
                }
            }
        }

        global::Owin.IAppBuilder IApplicationExtender.Configure(global::Owin.IAppBuilder app)
        {
            return app.UseTokenAuthentication(new TokenAuthenticationOptions());
        }

        private ClaimsPrincipal CreatePrincipal()
        {
            var identity = new ClaimsIdentity("Null", ClaimTypes.NameIdentifier, ClaimTypes.Role);
            identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, "scott"));
            identity.AddClaim(new Claim(ClaimTypes.Role, "Tester"));
            identity.AddClaim(new Claim(ClaimTypes.Role, "developer"));
            return new ClaimsPrincipal(identity);
        }

        private ClaimsPrincipal CreatePrincipal(string username, params string[] groups)
        {
            var identity = new ClaimsIdentity("Null", ClaimTypes.NameIdentifier, ClaimTypes.Role);
            identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, username));
            foreach (var group in groups)
            {
                identity.AddClaim(new Claim(ClaimTypes.Role, group));
            }
            
            return new ClaimsPrincipal(identity);
        }
    }
    

    
}

/*
 
 
                    
                    
                    
//                    var cookieJar = new KeyValuePairCookieJar(new SecureCookieJarSettings
//                                                              {
//                                                                  ApplicationSaltSettings = new TestStartup()
//                                                                      .CreateTestConfig("")
//                                                                      .GetValue(CommonSettings.SaltKey)
//                                                              });
                    
//                    var map = new MultiMap<string, string>();
//                    map.Add(TokenAuthenticationMiddleware.NameKey, "scott");
//                    map.Add(TokenAuthenticationMiddleware.GroupKey, "Youngblut");
//                    map.Add(TokenAuthenticationMiddleware.GroupKey, "Developer");
//                    cookieJar.SetSecureCookieValues("user", map);
//                    string value = cookieJar.GetCookieValue("user");
//
//                    message.Headers.Add("Cookie", "user=" + value);
 
 
 */
