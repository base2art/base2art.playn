﻿namespace Base2art.PlayN.Http.Owin.Security
{
    using System;
	using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Security.Claims;
    using Base2art.Collections.Generic;
    using Base2art.ComponentModel;
    using Base2art.Net;
    using Base2art.PlayN.Api;
    using Base2art.PlayN.Api.Config;
    using Base2art.PlayN.Security;
    using FluentAssertions;
    using Microsoft.Owin.Testing;
    using NUnit.Framework;
    using Base2art.PlayN.Http.Owin.Security.Fixtures;

    [TestFixture]
    public class FomsAuthMockFeature : AppBaseFeature, IApplicationExtender
    {
        [Test]
        public async void ShouldAuthenticateCorrectlyAndSaveDataToCookie()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<IApplicationExtender>().To(x => this);
            loader.Bind<IHttpUserLookup>().To(x => new MockDatabaseHttpUserLookup());
            loader.Bind<IRouter>().To(x => CreateRouter());
            
            using (Using.TransactionallyAssignTo(loader, () => ServiceLoader.PrimaryLoader))
            {
                using (var server = TestServer.Create<TestStartup>())
                {
                    HttpRequestMessage message = new HttpRequestMessage(
                        HttpMethod.Get,
                        new Uri("http://localhost/print-user"));
                    //?username=scott
                    
                    
                    var cookieJar = new KeyValuePairCookieJar(new SecureCookieJarSettings
                                                              {
                                                                  ApplicationSaltSettings = new TestStartup()
                                                                      .CreateTestConfig("")
                                                                      .GetValue(CommonSettings.SaltKey),
                                                                  SecureCookiePrefix = "scs"
                                                              });
                    
                    var map = new MultiMap<string, string>();
                    map.Add(TokenAuthenticationMiddleware.NameKey, "scott");
                    cookieJar.SetSecureCookieValues("user", map);
                    string value = cookieJar.GetCookieValue("user");
                    
                    message.Headers.Add("Cookie", "user=" + value);
                    
                    HttpResponseMessage response = await server.HttpClient.SendAsync(message);
                    (await response.Content.ReadAsStringAsync()).Should().Be("scott[developer,Tester]True");
                    
                    response.Headers.GetValues("Set-Cookie")
                        .First(x=>x.StartsWith("user="))
                        .Should()
                        .Be("user=%2Fscs%2F8CA209FBD3232725203C7FC7A911F1B3%3Fn%3Dscott; path=/");
                }
            }
        }
        
        [Test]
        public async void ShouldMergeWindowsAuthWithFormsAuth()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<IApplicationExtender>().To(x => new ClaimsPrincipalSetup());
            loader.Bind<IApplicationExtender>().To(x => this);
            loader.Bind<IHttpUserLookup>().To(x => new MockDatabaseHttpUserLookup2());
            loader.Bind<IRouter>().To(x => CreateRouter());
            
            using (Using.TransactionallyAssignTo(loader, () => ServiceLoader.PrimaryLoader))
            {
                using (var server = TestServer.Create<TestStartup>())
                {
                    HttpRequestMessage message = new HttpRequestMessage(
                        HttpMethod.Get,
                        new Uri("http://localhost/print-user"));
                    //?username=scott
                    
                    
                    var cookieJar = new KeyValuePairCookieJar(new SecureCookieJarSettings
                                                              {
                                                                  ApplicationSaltSettings = new TestStartup()
                                                                      .CreateTestConfig("")
                                                                      .GetValue(CommonSettings.SaltKey)
                                                              });
                    
                    var map = new MultiMap<string, string>();
                    map.Add(TokenAuthenticationMiddleware.NameKey, "scott");
                    cookieJar.SetSecureCookieValues("user", map);
                    string value = cookieJar.GetCookieValue("user");
                    
                    message.Headers.Add("Cookie", "user=" + value);
                    
                    using (CreatePrincipal().Assign().TransactionallyTo(() => ClaimsPrincipalSetup.Principal))
                    {
                        HttpRequestMessage message1 = new HttpRequestMessage(
                            HttpMethod.Get,
                            new Uri("http://localhost/print-user"));
                        
                        HttpResponseMessage response = await server.HttpClient.SendAsync(message1);
                        (await response.Content.ReadAsStringAsync()).Should().Be("scott[developer,Tester]True");
                        var cookies = response.Headers.GetValues("Set-Cookie");
                        
                        
                        response.Headers.GetValues("Set-Cookie")
                            .First(x=>x.StartsWith("user="))
                            .Should()
                            .Be("user=%2Fscs%2FE7954564E97BADC1539A40C4556ED432%3Fn%3Dscott%26g%3DTester; path=/");
                    }
                }
            }
        }
        
        private ClaimsPrincipal CreatePrincipal()
        {
            var identity = new ClaimsIdentity("Null", ClaimTypes.NameIdentifier, ClaimTypes.Role);
            identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, "scott"));
            identity.AddClaim(new Claim(ClaimTypes.Role, "Tester"));
            return new ClaimsPrincipal(identity);
        }

        global::Owin.IAppBuilder IApplicationExtender.Configure(global::Owin.IAppBuilder app)
        {
            return app.UseTokenAuthentication(new TokenAuthenticationOptions());
        }
        
        private class MockDatabaseHttpUserLookup : IHttpUserLookup
        {
            public IHttpUser FindUser(System.Security.Principal.IPrincipal principal)
            {
                return new HttpUser(principal.Identity.Name, new string[]{"developer", "Tester"});
            }
        }
        
        private class MockDatabaseHttpUserLookup2 : IHttpUserLookup
        {
            public IHttpUser FindUser(System.Security.Principal.IPrincipal principal)
            {
                List<string> groups = new List<string>{"developer"};
                
                var claimsPrincipal = principal as ClaimsPrincipal;
                if (claimsPrincipal != null)
                {
                    var roles = claimsPrincipal.Identities
                        .SelectMany(y => y.Claims
                                    .Where(x => x.Type == y.RoleClaimType)
                                    .Select(x => x.Value))
                        .ToArray();
                    groups.AddRange(roles);
                }
                
                return new HttpUser(
                    principal.Identity.Name,
                    groups.ToArray());
            }
        }
    }
}


/*
 
 
                    
                    
                    
//                    var cookieJar = new KeyValuePairCookieJar(new SecureCookieJarSettings
//                                                              {
//                                                                  ApplicationSaltSettings = new TestStartup()
//                                                                      .CreateTestConfig("")
//                                                                      .GetValue(CommonSettings.SaltKey)
//                                                              });
                    
//                    var map = new MultiMap<string, string>();
//                    map.Add(TokenAuthenticationMiddleware.NameKey, "scott");
//                    map.Add(TokenAuthenticationMiddleware.GroupKey, "Youngblut");
//                    map.Add(TokenAuthenticationMiddleware.GroupKey, "Developer");
//                    cookieJar.SetSecureCookieValues("user", map);
//                    string value = cookieJar.GetCookieValue("user");
//
//                    message.Headers.Add("Cookie", "user=" + value);
 
 
 */

