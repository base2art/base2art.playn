namespace Base2art.PlayN.Api
{
    using Base2art.PlayN.Mvc;

    public interface INotPossibleController : IRenderingController
    {
    }
}