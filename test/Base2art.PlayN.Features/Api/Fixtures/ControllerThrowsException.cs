﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Base2art.PlayN.Api.Fixtures
{
    using Base2art.PlayN.Api;
    using Base2art.PlayN.Http;
    using Base2art.PlayN.Mvc;

    public class ControllerThrowsException:IRenderingController
    {
        public IPositionedRenderingController[] RenderingControllers
        {
            get
            {
                return new IPositionedRenderingController[0];
            }
        }

        public INonRenderingController[] NonRenderingControllers
        {
            get
            {
                return new INonRenderingController[0];
            }
        }

        public IResult Execute(IHttpContext httpContext, List<PositionedResult> childResults)
        {
            throw new NotImplementedException();
        }
    }
}
