namespace Base2art.PlayN.Mvc
{
    using Base2art.PlayN.Api;
    using Base2art.PlayN.Api.Diagnostics;
    using Base2art.PlayN.Http;

    public class TestHttpContext : IHttpContext
    {
        private readonly TestHttpResponse response = new TestHttpResponse();

        private readonly InMemoryLogger logger = new InMemoryLogger(LogLevels.Always);
        
        public IApplication ApplicationInstance
        {
            get
            {
                throw new System.NotImplementedException();
            }
        }

        public ILogger Logger
        {
            get
            {
                return this.logger;
            }
        }

        IHttpFlash IHttpContext.Flash
        {
            get
            {
                throw new System.NotImplementedException();
            }
        }

        IHttpSession IHttpContext.Session
        {
            get
            {
                throw new System.NotImplementedException();
            }
        }

        IHttpResponse IHttpContext.Response
        {
            get
            {
                return this.Response;
            }
        }

        IHttpRequest IHttpContext.Request
        {
            get
            {
                throw new System.NotImplementedException();
            }
        }

        public TestHttpResponse Response
        {
            get
            {
                return this.response;
            }
        }
    }
}