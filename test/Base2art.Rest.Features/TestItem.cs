﻿namespace Base2art.Rest
{
    public class TestItem
    {
        private readonly int i;

        public TestItem(int i)
        {
            this.i = i;
        }

        public int I
        {
            get
            {
                return this.i;
            }
        }
    }
}