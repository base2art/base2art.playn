﻿namespace Base2art.Rest
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Runtime.Serialization.Formatters.Binary;

    using Base2art.Rest.Validation;

    using FluentAssertions;

    using NUnit.Framework;

    [TestFixture]
    public class ValidationTest
    {
        private const string AmountErrorString = "The total amount of bill should equal the price of the meal plus the tip.";

        private const string IntegrityErrorString = "You cannot delete this address because every person must have one address "
           + "and deleting this address would cause 'Sarah Johnson' to not have an address.";

        [Test]
        public void ShouldCreateException()
        {
            ValidationDataCollection isolated = new ValidationDataCollection();
            ValidationDataCollection all = new ValidationDataCollection();

            isolated.HasErrors.Should().BeFalse();

            isolated.AddFieldError("name", ValidationDataStandarErrorTypes.Required, "Name is a required field");
            isolated.FieldErrorData.Count.Should().Be(1);
            isolated.HasErrors.Should().BeTrue();
            isolated.AddFieldError("name", ValidationDataStandarErrorTypes.Required.ToString("G"), "Name is a required field");
            isolated.FieldErrorData.Count.Should().Be(2);
            isolated.HasErrors.Should().BeTrue();

            isolated = new ValidationDataCollection();
            isolated.AddGlobalError("PermissionError", "You do not have permission to perform this operation");
            isolated.GlobalErrorData.Count.Should().Be(1);
            isolated.AddGlobalError(ValidationDataStandarErrorTypes.PermissionError, "You do not have permission to perform this operation");
            isolated.GlobalErrorData.Count.Should().Be(2);
            isolated.HasErrors.Should().BeTrue();

            isolated = new ValidationDataCollection();
            isolated.AddObjectError("CostTotalError", AmountErrorString);
            isolated.ObjectErrorData.Count.Should().Be(1);
            isolated.AddObjectError(ValidationDataStandarErrorTypes.PermissionError, AmountErrorString);
            isolated.ObjectErrorData.Count.Should().Be(2);
            isolated.HasErrors.Should().BeTrue();

            isolated = new ValidationDataCollection();
            isolated.AddSystemError("AddressIntegrityError", IntegrityErrorString);
            isolated.SystemErrorData.Count.Should().Be(1);
            isolated.AddSystemError(ValidationDataStandarErrorTypes.IntegrityError, IntegrityErrorString);
            isolated.SystemErrorData.Count.Should().Be(2);
            isolated.HasErrors.Should().BeTrue();

            all.AddFieldError("name", ValidationDataStandarErrorTypes.Required, "Name is a required field");
            all.AddGlobalError(ValidationDataStandarErrorTypes.PermissionError, "You do not have permission to perform this operation");
            all.AddObjectError(ValidationDataStandarErrorTypes.PermissionError, AmountErrorString);
            all.AddSystemError(ValidationDataStandarErrorTypes.IntegrityError, IntegrityErrorString);

            all.Count.Should().Be(4);
            try
            {
                throw new DataValidationException(all);
            }
            catch (DataValidationException e)
            {
                IValidationDataCollection collection = e.Messages;
                collection.HasErrors.Should().BeTrue();
                collection.Count.Should().Be(4);
                int i = 0;

                for (IEnumerator<IValidationData> it = collection.AllErrors.GetEnumerator(); it.MoveNext(); )
                {
                    var throwAway = it.Current;
                    i += 1;
                }

                i.Should().Be(4);
            }
        }

        [Test]
        public void ShouldSerializeAndDeserialize()
        {
            ValidationDataCollection all = new ValidationDataCollection();
            all.AddFieldError("name", ValidationDataStandarErrorTypes.Required, "Name is a required field");
            all.AddGlobalError(ValidationDataStandarErrorTypes.PermissionError, "You do not have permission to perform this operation");
            all.AddObjectError(ValidationDataStandarErrorTypes.PermissionError, AmountErrorString);
            all.AddSystemError(ValidationDataStandarErrorTypes.IntegrityError, IntegrityErrorString);

            var dto = new DataValidationException(all);
            var obj = this.SerializeDeserialize(dto);
            obj.Messages.AllErrors.Count().Should().Be(4);
        }

        [Test]
        public void ShouldDisplayCorrectMessageWithNoErrors()
        {
            ValidationDataCollection all = new ValidationDataCollection();
            var dto = new DataValidationException(all, new ArgumentOutOfRangeException("Bad News"));
            dto.Message.Should().Be("Generic Error");
        }

        [Test]
        public void ShouldSerializableHaveDataAccessApi()
        {
            var dto1 = new DataAccessException();
            this.SerializeDeserialize(dto1);

            var dto2 = new DataAccessException("Custom Message");
            this.SerializeDeserialize(dto2).Message.Should().Be("Custom Message");

            var dto3 = new DataAccessException("Custom Message", new FileNotFoundException());
            var dataAccessException = this.SerializeDeserialize(dto3);
            dataAccessException.Message.Should().Be("Custom Message");
            dataAccessException.InnerException.Should().BeOfType<FileNotFoundException>();
        }

        private T SerializeDeserialize<T>(T obj)
        {
            using (MemoryStream mem = new MemoryStream())
            {
                BinaryFormatter b = new BinaryFormatter();

                b.Serialize(mem, obj);
                mem.Seek(0L, SeekOrigin.Begin);
                return (T)b.Deserialize(mem);
            }
        }
    }
}
