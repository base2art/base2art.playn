﻿namespace Base2art.Rest
{
    using System;
    using System.Linq;

    using Base2art.Rest.Models;

    using FluentAssertions;

    using NUnit.Framework;

    [TestFixture]
    public class PaginationFeature
    {
        [Test]
        public void ShouldValidateItemsSizeIsLessThanEqualToRequestedPageSize()
        {
            var items = CreateTestItems(8);

            PaginationData paginationData = new PaginationData(0, 5);
            new Action(() => new PagedList<TestItem>(items, paginationData, items.Length))
                .ShouldThrow<ArgumentException>();
        }

        [Test]
        public void ShouldHaveBasicProp()
        {
            var items = CreateTestItems(8);

            PaginationData paginationData = new PaginationData();
            paginationData.PageIndex = 0;
            paginationData.PageSize = 5;
            var list = new PagedList<TestItem>(items.Take(5).ToArray(), paginationData, items.Length);
            int count = 0;
            list.ForAll(x => count++);
            count.Should().Be(5);
            list.Data.Length.Should().Be(5);
            list.Count.Should().Be(5);
            list.PaginationInformation.TotalCount.Should().Be(8);
        }

        [Test]
        public void ShouldExecutePagingTests()
        {
            TestItem[] items = new TestItem[0];
            PaginationData pdata = new PaginationData();
            pdata.PageIndex = 0;
            pdata.PageSize = 10;
            PagedList<TestItem> list = new PagedList<TestItem>(items, pdata, 100);

            IPaginationInformation info = list.PaginationInformation;
            this.AssertPagination(info, false, true, 0, 9);
            info = info.NextPage;
            this.AssertPagination(info, true, true, 10, 19);
            info = info.NextPage;
            this.AssertPagination(info, true, true, 20, 29);
            info = info.NextPage;
            this.AssertPagination(info, true, true, 30, 39);
            info = info.NextPage;
            this.AssertPagination(info, true, true, 40, 49);
            info = info.NextPage;
            this.AssertPagination(info, true, true, 50, 59);
            info = info.NextPage;
            this.AssertPagination(info, true, true, 60, 69);
            info = info.NextPage;
            this.AssertPagination(info, true, true, 70, 79);
            info = info.NextPage;
            this.AssertPagination(info, true, true, 80, 89);
            info = info.NextPage;
            this.AssertPagination(info, true, false, 90, 99);

            info = info.PreviousPage;
            this.AssertPagination(info, true, true, 80, 89);
            info = info.PreviousPage;
            this.AssertPagination(info, true, true, 70, 79);
            info = info.PreviousPage;
            this.AssertPagination(info, true, true, 60, 69);
            info = info.PreviousPage;
            this.AssertPagination(info, true, true, 50, 59);
            info = info.PreviousPage;
            this.AssertPagination(info, true, true, 40, 49);
            info = info.PreviousPage;
            this.AssertPagination(info, true, true, 30, 39);
            info = info.PreviousPage;
            this.AssertPagination(info, true, true, 20, 29);
            info = info.PreviousPage;
            this.AssertPagination(info, true, true, 10, 19);
            info = info.PreviousPage;
            this.AssertPagination(info, false, true, 0, 9);
        }

        [Test]
        public void ShouldExecutePagingTest2()
        {
            TestItem[] items = new TestItem[0];
            PaginationData pdata = new PaginationData();
            pdata.PageIndex = 0;
            pdata.PageSize = 10;
            PagedList<TestItem> list = new PagedList<TestItem>(items, pdata, 95);

            IPaginationInformation info = list.PaginationInformation;
            this.AssertPagination(info, false, true, 0, 9);
            info = info.NextPage;
            this.AssertPagination(info, true, true, 10, 19);
            info = info.NextPage;
            this.AssertPagination(info, true, true, 20, 29);
            info = info.NextPage;
            this.AssertPagination(info, true, true, 30, 39);
            info = info.NextPage;
            this.AssertPagination(info, true, true, 40, 49);
            info = info.NextPage;
            this.AssertPagination(info, true, true, 50, 59);
            info = info.NextPage;
            this.AssertPagination(info, true, true, 60, 69);
            info = info.NextPage;
            this.AssertPagination(info, true, true, 70, 79);
            info = info.NextPage;
            this.AssertPagination(info, true, true, 80, 89);
            info = info.NextPage;
            this.AssertPagination(info, true, false, 90, 94);

            info = info.PreviousPage;
            this.AssertPagination(info, true, true, 80, 89);
            info = info.PreviousPage;
            this.AssertPagination(info, true, true, 70, 79);
            info = info.PreviousPage;
            this.AssertPagination(info, true, true, 60, 69);
            info = info.PreviousPage;
            this.AssertPagination(info, true, true, 50, 59);
            info = info.PreviousPage;
            this.AssertPagination(info, true, true, 40, 49);
            info = info.PreviousPage;
            this.AssertPagination(info, true, true, 30, 39);
            info = info.PreviousPage;
            this.AssertPagination(info, true, true, 20, 29);
            info = info.PreviousPage;
            this.AssertPagination(info, true, true, 10, 19);
            info = info.PreviousPage;
            this.AssertPagination(info, false, true, 0, 9);
        }

        private static TestItem[] CreateTestItems(int count)
        {
            TestItem[] items = new TestItem[count];

            for (int i = 0; i < count; i++)
            {
                items[i] = new TestItem(i);
            }

            return items;
        }

        private void AssertPagination(IPaginationInformation info, bool previous, bool next, int start, int end)
        {
            info.HasPreviousPage.Should().Be(previous);
            info.HasNextPage.Should().Be(next);

            info.Start.Should().Be(start);
            info.End.Should().Be(end);
        }
    }
}
